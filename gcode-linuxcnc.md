# G-Code: LinuxCNC

 a LinuxCNC G-Code command list


## References

 - http://linuxcnc.org/docs/html/gcode.html


## G-Code

 For a description of every code command read the LinuxCNC reference!


### Motion

 Parameters X, Y, Z, A, B, C, U, V, W apply to all motions

 | Code              | Parameters   | Description                 |
 | ----------------- | ------------ | --------------------------- |
 | `G0`              |              | Rapid Move                  |
 | `G1`              |              | Linear Move                 |
 | `G2`              | I J K or R P | Arc Move clockwise          |
 | `G3`              | I J K or R P | Arc Move counterclockwise   |
 | `G4`              | P            | Dwell                       |
 | `G5`              | I J P Q      | Cubic Spline                |
 | `G5.1`            | I J          | Quadratic Spline            |
 | `G5.2`            | P L          | NURBS                       |
 |                   |              |                             |
 | `G33`             | K            | Spindle Synchronized Motion |
 | `G33.1`           | K            | Rigid Tapping               |
 | `G38.2` - `G38.5` |              | Stright Probe               |
 |                   |              |                             |
 | `G80`             |              | Cancel Canned Cycle         |


### Canned Cycles

 Parameters X, Y, Z or U, V, W apply to canned cycles, depending on active plane

 | Code   | Parameters          | Description                   |
 | ------ | ------------------- | ----------------------------- |
 | `G73`  | R L Q               | Drilling Cycle, Chip Breaking |
 |        |                     |                               |
 | `G76`  | P Z I J R K Q H L E | Threading Cycle               |
 |        |                     |                               |
 | `G81`  | R L (P)             | Drilling Cycle                |
 | `G82`  | R L (P)             | Drilling Cycle, Dwell         |
 | `G83`  | R L Q               | Drilling Cycle. Peck          |
 |        |                     |                               |
 | `G85`  | R L (P)             | Boring Cycle, Feed Out        |
 | `G89`  | R L (P)             | Boring Cycle, Dwell, Feed Out |


### Distance Mode

 | Code    | Parameters | Description         |
 | ------- | ---------- | ------------------- |
 | `G7`    |            | Lathe Diameter Mode |
 | `G8`    |            | Lathe Radius Mode   |
 |         |            |                     |
 | `G90`   |            | Distance Mode       |
 | `G90.1` |            | Arc Distance Mode   |
 | `G91`   |            | Distance Mode       |
 | `G91.1` |            | Arc Distance Mode   |


### Feed Rate Mode

 | Code   | Parameters | Description    |
 | ------ | ---------- | -------------- |
 | `G93`  |            | Feed Rate Mode |
 | `G94`  |            | Feed Rate Mode |
 | `G95`  |            | Feed Rate Mode |


### Spindle Control

 | Code   | Parameters| Description          |
 | ------ | --------- | -------------------- |
 | `M3`   | S         | Spindle Control      |
 | `M4`   | S         | Spindle Control      |
 | `M5`   | S         | Spindle Control      |
 |        |           |                      |
 | `M19`  |           | Orient Spindle       |
 |        |           |                      |
 | `G96`  | S D       | Spindle Control Mode |
 | `G97`  | S D       | Spindle Control Mode |


### Coolant

 | Code   | Parameters | Description     |
 | ------ | ---------- | --------------- |
 | `M7`   |            | Coolant Control |
 | `M8`   |            | Coolant Control |
 | `M9`   |            | Coolant Control |


### Tool Length Offset

 | Code    | Parameters | Description                         |
 | ------- | ---------- | ----------------------------------- |
 | `G43`   | H          | Tool Length Offset                  |
 | `G43.1` |            | Dynamic Tool Length Offset          |
 | `G43.2` | H          | Apply additional Tool Length Offset |
 |         |            |                                     |
 | `G49`   |            | Cancel Tool Length Compensation     |


### Stopping

 | Code   | Parameters | Description         |
 | ------ | ---------- | ------------------- |
 | `M0`   |            | Program Pause       |
 | `M1`   |            | Program Pause       |
 | `M2`   |            | Program End         |
 | `M30`  |            | Program End         |
 | `M60`  |            | Pallet Change Pause |


### Units

 | Code   | Parameters | Description | Annotation      |
 | ------ | ---------- | ----------- | --------------- |
 | `G20`  |            | Unit "inch" | WinPC-NC: `G70` |
 | `G21`  |            | Unit "mm"   | WinPC-NC: `G71` |


### Plane Selection

 Affects G2, G3, G40 ... G42, G81 ... G89


 | Code            | Parameters | Description  |
 | --------------- | ---------- | ------------ |
 | `G17` - `G19.1` |            | Plane Select |


### Cutter Radius Compensation

 | Code    | Parameters | Description                 |
 | ------- | ---------- | --------------------------- |
 | `G40`   |            | Compensation Off            |
 | `G41`   | D          | Cutter Compensation         |
 | `G41.1` | D L        | Dynamic Cutter Compensation |
 | `G42`   | D          | Cutter Compensation         |
 | `G42.1` | D L        | Dynamic Cutter Compensation |


### Path Control Mode

 | Code    | Parameters | Description     |
 | ------- | ---------- | --------------- |
 | `G61`   |            | Exact Path Mode |
 | `G61.1` |            | Exact Path Mode |
 | `G64`   | P Q        | Path Blending   |


### Return Mode in Canned Cycles

 | Code    | Parameters | Description               |
 | ------- | ---------- | ------------------------- |
 | `G98`   |            | Canned Cycle Return Level |
 | `G99`   |            | Canned Cycle Return Level |


### Other Modal Codes

 | Code            | Parameters          | Description                     |
 | --------------- | ------------------- | ------------------------------- |
 | `F`             |                     | Set Feed Rate                   |
 | `S`             |                     | Set Spindle Speed               |
 | `T`             |                     | Select Tool                     |
 |                 |                     |                                 |
 | `M48`           |                     | Speed and Feed Override Control |
 | `M49`           |                     | Speed and Feed Override Control |
 | `M50`           | P0 (off) or P1 (on) | Feed Override Control           |
 | `M51`           | P0 (off) or P1 (on) | Spindle Speed Override Control  |
 | `M52`           | P0 (off) or P1 (on) | Adaptive Feed Control           |
 | `M53`           | P0 (off) or P1 (on) | Feed Stop Control               |
 |                 |                     |                                 |
 | `G54` - `G59.3` |                     | Select Coordinate System        |


### Flow-Control Codes

 | Command    | Description                           |
 | ---------- | ------------------------------------- |
 | `o sub`    | Subroutines, sub / endsub call        |
 | `o while`  | Looping, while / endwhile, do / while |
 | `o if`     | Conditional,  if / else / eindif      |
 | `o repeat` | Repeat a loop of code                 |
 | `o call`   | Call named file                       |
 | `[]`       | Indirection                           |


 | Code   | Parameters | Description                       |
 | ------ | ---------- | --------------------------------- |
 | `M70`  |            | Save modal state                  |
 | `M71`  |            | Invalidate stored state           |
 | `M72`  |            | Restore modal state               |
 | `M73`  |            | Save and Auto-restore modal state |


### Input/Output Codes

 | Code          | Parameters | Description                 |
 | ------------- | ---------- | --------------------------- |
 | `M62` - `M65` | P          | Digital Output Control      |
 | `M66`         | P E L Q    | Wait for Input              |
 | `M67`         | T          | Analog Output, Synchronized |
 | `M68`         | T          | Analog Output, Immediate    |


### Non-Modal Codes

 | Code             | Parameters | Description                 |
 | ---------------- | ---------- | --------------------------- |
 | `M6`             | T          | Tool Change                 |
 | `M61`            | Q          | Set Current Tool            |
 |                  |            |                             |
 | `G10L1`          | P R Q      | Set Tool Table              |
 | `G10L10`         | P          | Set Tool Table              |
 | `G10L11`         | P          | Set Tool Table              |
 | `G10L2`          | P R        | Set Coordinate System       |
 | `G10L20`         | P          | Set Coordinate System       |
 |                  |            |                             |
 | `G28`, `G28.1`   |            | Go/Set Predefined Position  |
 | `G30`, `G30.1`   |            | Go/Set Predefined Position  |
 |                  |            |                             |
 | `G53`            |            | Move in Machine Coordinates |
 |                  |            |                             |
 | `G92`            |            | Coordinate System Offset    |
 | `G92.1`, `G92.2` |            | Reset G92 Offsets           |
 | `G92.3`          |            | Restore G92 Offsets         |
 |                  |            |                             |
 | `M101` - `M199`  | P Q        | User Defined Commands       |


### Comments & Messages

 | Code           | Parameters | Description    |
 | -------------- | ---------- | -------------- |
 | `; (...)`      |            | Comments       |
 | `(MSG, ...)`   |            | Messages       |
 | `(DEBUG, ...)` |            | Debug Messages |
 | `(PRINT, ...)` |            | Print Messages |

