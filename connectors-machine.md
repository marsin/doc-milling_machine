# Machine Connectors

 - Power Supply: DC 30V 3A


## Connector for System Supplies

 _(Source: EasyBuild Manual 2016-04-04)_

 | Pin    | Signal             |
 | -----: | :----------------- |
 |      1 | +24V               |
 |      2 | GND                |
 |      3 | +5V / VCC          |
 |      4 | DIR 4. Axis        |
 |      5 | Clock 4. Axis      |
 |      6 | Relais 2           |
 |      7 | PWM                |
 |      8 | WZ Lenght Sensor   |
 |      9 | +24V               |
 |     10 | GND                |
 |     11 | Disable            |
 |     12 | End Switch 4. Axis |
 |     13 | Relais 1           |
 |     14 | Relais 3           |
 |     15 | _free_ (in)        |
 | Shield | PE                 |

