Automatic Tool Changer
======================

 _Latest Update: 2020-10-16_


Hardware
--------

 - [HF-Spindel 500 W](https://shop.stepcraft-systems.com/hf-spindel-500-w)
 - [Automatischer Werkzeugwechsler HF 500](https://shop.stepcraft-systems.com/automatischer-werkzeugwechsler-hf-500)


Configuration
-------------

### WinPC-NC

 - Parameter
   - Grundeinstellungen
     - Zubehör
       - [x] Autom. Wechsler
     - Signalassistent
       - Ausgänge: "Q248 Spannzange" -> Leitung: "LPT1 Pin14"
     - Spindel
       - [x] Spannzange nach Einschalten geschlossen
             (damit funktioniert auch der Signaltest korrekt)
   - Werkzeuge
     - Wechslermagazin
       - [x] Wechsler, Z nicht hochfahren
   - Macro
     - Macroausführung bei ...
       - AutoWechsler Aufnehmen: (Macro nach Anleitung)
       - AutoWechsler Ablegen:   (Macro nach Anleitung)
