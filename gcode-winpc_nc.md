# G-Code: WinPC-NC

 a WinPC-NC G-Code command list


## References

 - https://www.lewetz.de/de/service/downloads
 - https://www.lewetz.de/files/download/hbwin3usbd.pdf
   - Capter 8.1 Interpreter, DIN/ISO Interpreter, Page 162


## G-Code

 The following commands are supported


### G-Commands

 | Code      | Parameters            | Description                                  | Annotation           |
 | --------- | --------------------- | -------------------------------------------- | -------------------- |
 | `G00`     |                       | Rapid Move with Linear Interpolation         |                      |
 | `G01`     |                       | Process, Linear Interpolation                |                      |
 | `G02`     | `I`, `J`, `K` or `R`  | Arc Move Clockwise                           |                      |
 | `G03`     | `I`, `J`, `K` or `R`  | Arc Move Counterclockwise                    |                      |
 | `G04`     | `F`: dwell time in ms | Dwell                                        | _(LinuxCNC: `P`)_    |
 |           |                       |                                              |                      |
 | `G17`     |                       | Plane XY                                     | _(used for arc)_     |
 | `G18`     |                       | Plane XZ                                     | _(used for arc)_     |
 | `G19`     |                       | Plane YZ                                     | _(used for arc)_     |
 |           |                       |                                              |                      |
 | `G28`     |                       | Goto Zero Point                              |                      |
 |           |                       |                                              |                      |
 | `G54..59` |                       | Zero Point Offset                            |                      |
 |           |                       |                                              |                      |
 | `G70`     |                       | Unit in _inch_                               | _(LinuxCNC: `G20`)_  |
 | `G71`     |                       | Unit in _mm_                                 | _(LinuxCNC: `G21`)_  |
 |           |                       |                                              |                      |
 | `G81`     | `Z`: drilling depth   | Drilling Cycle                               |                      |
 |           | `R`: pullback height  |   with pullback in feed speed                |                      |
 |           | `P`: Dwell            |                                              |                      |
 | `G82`     | `Z`: drilling depth   | Drilling Cycle                               |                      |
 |           | `R`: pullback height  |   with pullback in rapid speed               |                      |
 |           | `P`: Dwell            |                                              |                      |
 |           |                       |                                              |                      |
 | `G90`     |                       | Absolute Path Values                         |                      |
 | `G91`     |                       | Relative Path Values                         |                      |
 |           |                       |                                              |                      |
 | `G92`     |                       | 3D Printing - resetting spending of filament | Compatible with CURA |
 |           |                       |                                              |                      |
 | `G98`     |                       | Defining a Subroutine                        |                      |


### M-Functions

 | Code         | Parameters        | Description                        | Annotation                                 |
 | ------------ | ----------------- | ---------------------------------- | ------------------------------------------ |
 | `M00`        |                   | Program Halt                       | _(a button "Continue" appears)_            |
 | `M02`        |                   | Program End                        | _(what's the difference to `M30`?)_        |
 |              |                   |                                    |                                            |
 | `M03`        | `S`               | Spindle On, Clockwise              |                                            |
 | `M04`        | `S`               | Spindle On, Counterclockwise       |                                            |
 | `M05`        |                   | Spindle Off                        |                                            |
 | `M06`        |                   | Tool Change                        | _(define Tool with `T`)_                   |
 |              |                   |                                    |                                            |
 | `M07`        |                   | Coolant On                         | _(LinuxCNC: Mist)_                         |
 | `M08`        |                   | Coolant On                         | _(LinuxCNC: Flood)_                        |
 | `M09`        |                   | Coolant Off                        |                                            |
 |              |                   |                                    |                                            |
 | `M16`        | `F`: input number | Waiting for Input                  | _(ignored, when input was not configured)_ |
 | `M30`        |                   | Program End                        | _(what's the difference to `M02`?)_        |
 | `M66`        |                   | Tool Change                        | same as `M06`                              |
 |              |                   |                                    |                                            |
 | `M70..77`    |                   | Set Output 100 - 107 HIGH          |                                            |
 | `M-70..M-77` |                   | Set Output 100 - 107 LOW           |                                            |
 | `M80..87`    |                   | Set Output 108 - 115 HIGH          |                                            |
 | `M-80..M-87` |                   | Set Output 108 - 115 LOW           |                                            |
 |              |                   |                                    |                                            |
 | `M90..99`    |                   | Activates Programmable Macros 1-10 |                                            |
 |              |                   |                                    |                                            |
 | `M106`       |                   | Set Output for 3D-Print Head ON    |                                            |
 | `M107`       |                   | Set Output for 3D-Print Head OFF   |                                            |


### Other Commands

 | Code          | Description                                 | Annotation                          |
 | ------------- | ------------------------------------------- | ----------------------------------- |
 | `N`           | Sequence Number                             | _(like in BASIC)_                   |
 | `S`           | Set Spindle Speed                           |                                     |
 | `F`           | Input Number                                | with `M16` command                  |
 | `F`           | Dwell                                       | with `G04` command                  |
 | `F`           | Feed Rate in _mm/s_, _mm/min_ or _inch/min_ | without `M16` and `G04`             |
 |               |                                             |                                     |
 | `I`, `J`, `K` | Arc Parameter                               |                                     |
 |               |                                             |                                     |
 | `T`           | Tool Selection                              | up to 30 Tools _(WinPC-NC 3.40/05)_ |
 | `X`, `Y`, `Z` | Coordinates                                 |                                     |
 | `A`, `B`, `C` | Coordinates for 4th Axis                    | if active                           |
 | `R`           | Radius for Arcs                             |                                     |
 | `R`           | Pullback height at Drill Cycles             | `G81`, `G82`                        |
 | `P`           | Dwell time at Drill Cycles                  | `G81`, `G82`                        |
 |               |                                             |                                     |
 | `L`           | Call a subroutine                           | routine numbers 1..20               |


### Further More

 The following points must be observed when creating DIN/ISO programs:

 - The programs must be started using an editor or external program itself.

 - The program name with the leading `%` character directs the actual program code,
   all lines in front of it are comment lines.

 - At least one tool must be selected and changed,
   e.g. with `T1 M6` in the program header _(`M6` is mandatory)_.

 - The speeds can be set with `F` commands
   in the unit _mm/sec_ or _mm/min_ defined by parameter.

 - The command number can be defined by the user using N commands.
   For circular arcs, you can either use the commands `I`, `J`, `K`
   program the center point or a radius with `R`.
   Positive radii create an arc smaller than _180 degrees_
   and negative radii an arc larger than _180 degrees_.

 - Up to 20 subroutines are programmed with `G98 Lx`
   at the end of the Main program defined after `M30`.
   The definition ends with `G98 L0`.
   The subprogram is called at any position with `Lx`.

 - For the zero offset with `G54ff`,
   the definition of the zero point is done with the call line `G54 xxxx yyyy`,
   whereby the coordinate values the relative displacement to the current Define zero point.
   The zero point is activated by then with a single command `G54` without coordinates.


### Examples

#### Square with rounded corners

```
%prog1                  ; program start
N001 G90                ; absolute coordinates
N002 G71 T1 M6          ; units in mm, tool 1
N003 G00 X110 Y100 Z10  ; rapid move to first position
N004 G01 Z11            ; infeed with Z
N005 G01 X190           ; move stright forward with feed
N006 G03 X200 Y110 J10  ; arc of a circle around center point
N007 G01 Y190
N008 G03 X190 Y200 I-10
N009 G01 X110
N010 G03 X100 Y190 J-10
N011 G01 Y110
N012 G03 X110 Y100 I10
N013 G01 Z10
N014 G00 X0 Y0 Z0       ; rapid move to zero point
N015 M30                ; program end
```

#### Drill Cycle as Subroutine

```
%prog2             ; program start
N001 G90           ; absolute coordinates
N002 G00 X110 Y100 ; rapid move to first position
N003 L1            ; starting drilling cycle
N004 G00 Y110      ; next position
N005 L1
N006 G00 Y120
N007 L1
N008 G00 X0Y0
N009 M30           ; program end
N020 G98 L1        ; defining drilling cycle
N021 G01 Z20
N022 G00 Z0
N023 G01 Z25
N024 G00 Z0
N025 G01 Z30
N026 G00 Z0
N027 G98 L0        ; subroutine end
```

