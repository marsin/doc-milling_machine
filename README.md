# CNC Milling Machine

 - Private notes about milling with the STEPCRAFT 2 - D600
 - This document comes without any warranty!


## System

### Hardware

 - __Milling Machine__:        [STEPCRAFT-2/D.600 Construction Kit][lnk-machine]
 - __Module - Parallel Port__: [STEPCRAFT Parallel Module][lnk-parallel]                  _(planned to buy, for usage with LinuxCNC)_
 - __Module - 4-Axis__:        [STEPCRAFT 4-Axis Module][lnk-4axis]                       _(planned to buy later)_
 - __Spindle:__                [STEPCRAFT HF Spindle 500 W][lnk-spindle]
 - __Tool Changer__:           [STEPCRAFT Automatic Tool Changer HF Spindle][lnk-changer]
 - __Tool Lenght Sensor__:     [STEPCRAFT Tool Length Sensor TS-32][lnk-toolsensor]
 - __Air Compressor__:         [Stanley Kompressor, DN200/10/5 AIRBOSS][lnk-compressor]   _(for the Tool Changer)_


### Software

 - __Control Software - Windows__: [WinPC-NC, USB 3.40/26][lnk-winpcnc], [Documentation & Update][lnk-winpcnc_doc] _(WindowsXP  32bit, Intel Celleron 4)_
   - According to the description in the [WinPC-NC USB 3.0][lnk-winpcnc_doc] document
     the [USB-Modul][lnk-winpcnc_winusb] is built into the STEPCRAFT Machine _(Chapter 2.2 Installation, Page 15)_.
     This USB module becomes switched, when the parallel port module becomes plugged in.
 - __Control Software - Linux__:   [LinuxCNC][lnk-linuxcnc]                                                         _(Arch Linux 64bit, Intel Core i5 7th Gen.)_
   - LinuxCNC on USB seems not to work easy: http://wiki.linuxcnc.org/cgi-bin/wiki.pl?HardwareDesign#USB


### OS Setup

#### Windows XP

 - _no virus scanner installed_
 - _no extra firewall installed_
 - deactivated Windows firewall            _(Control Panel --> Window Firewall)_
 - deactivated automatic Windows updates   _(Control Panel --> Automatic Updates)_
 - deactivated screen saver                _(Control Panel --> Display --> Screen Saver)_
 - set __Energy Options__ to __Permanent__ _(Control Panel --> Power Options)_
   - Energy Scheme
     - __Shutdown Monitor__:  Never
     - __Shutdown Harddisk__: Never
     - __Standby__:           Never
   - Extended
     - __At pressing the power switch__:       Do nothing
     - __At pressing the switch for standby__: Do nothing
 - set the monitor pixel density to 96 DPI _(Control Panel --> Display --> Settings --> Extended --> General)_
   - useful for the WinPC-NC software


#### Arch Linux

 - _has not yet been used to control the machine_


## Settings

 - [My Settings: WinPC-NC](./settings-winpc_nc.md)
   - __ATTENTION__: The Spindle must be plugged in, on power and switched on, when starting WinPC-NC!
   - [File: System parameter](./settings/winpc_nc/system_parameters.wpi)
   - [File: Tool parameter](./settings/winpc_nc/tool_parameters.wpw)
   - [File: Settings CAM functions](./settings/winpc_nc/cam_functions.wpo) _(none defined yet - blind link)_
   - [File: Defined macros](./settings/winpc_nc/macros.mac)                _(none defined yet - blind link)_
   - [My First Test Drive](./coordinates-first_test_drive.md)

 - [My Settings: LinuxCNC](./settings-linuxcnc.md) _(not used yet - blind link)_


## Hardware

 - [Machine Connectors](./connectors-machine.md)


## Practice

### Materials and Tools

 - [Tool Selection](./tool_selection.md)


### Programming

 - [LinuxCNC G-Code Command List](./gcode-linuxcnc.md), a transcript of the [LinuxCNC - G-Code Quick Reference][lnk-linuxcnc_gcode]
 - [WinPC-NC G-Code Command List](./gcode-winpc_nc.md), a transcript of the [WinPC-NC USB 3.0 Handbook, Chapter 8.1 - Interpreter, DIN/ISO Interpreter][lnk-winpcnc_doc]


 --

   [lnk-machine]:        https://shop.stepcraft-systems.com/Buy-D600-CNC-Router-Construction-Kit    "Shop: STEPCRAFT-2/D.600 Construction Kit"
   [lnk-spindle]:        https://shop.stepcraft-systems.com/hf-spindle-500-w                        "Shop: HF Spindle 500 W"
   [lnk-changer]:        https://shop.stepcraft-systems.com/automatic-tool-changer-hf-spindle       "Shop: Automatic Tool Changer HF Spindle"
   [lnk-toolsensor]:     https://shop.stepcraft-systems.com/tool-length-sensor                      "Shop: Tool Length Sensor TS-32"
   [lnk-parallel]:       https://shop.stepcraft-systems.com/parallel-module-oem-package-no-software "Shop: Parallel Module"
   [lnk-4axis]:          https://shop.stepcraft-systems.com/4-axis-module                           "Shop: 4-Axis Module (dseries)"
   [lnk-compressor]:     https://www.amazon.de/Stanley-Kompressor-DN200-10-AIRBOSS/dp/B00GN89EQE    "Amazon: Stanley Kompressor, DN200/10/5 AIRBOSS"

   [lnk-winpcnc]:        https://www.cnc-step.com/cnc-software/winpc-nc/                         "Developer: WinPC-NC"
   [lnk-winpcnc_doc]:    https://www.lewetz.de/de/service/downloads                              "Developer: WinPC-NC - Documentation"
   [lnk-winpcnc_winusb]: https://www.lewetz.de/de/sample-sites-2/winpc-nc/winusb                 "Developer: WinPC-NC - WinUSB Module"
   [lnk-linuxcnc]:       http://linuxcnc.org/                                                    "Project: LinuxCNC"
   [lnk-linuxcnc_gcode]: http://linuxcnc.org/docs/html/gcode.html                                "Project: LinuxCNC - G-Code Quick Reference"
   [lnk-linuxcnc_wiki]:  http://wiki.linuxcnc.org/cgi-bin/wiki.pl                                "Project: LinuxCNC - Wiki"
   [lnk-linuxcnc_forum]: https://forum.linuxcnc.org/                                             "Project: LinuxCNC - Forum"

