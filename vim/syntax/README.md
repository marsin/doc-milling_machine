# Vim Syntax Highlighting for NC codes

## Installation NVIM

 1. Copy the `nc.vim` file to `~/.config/nvim/syntax/nc.vim`
 2. Add the line `source ~/.config/nvim/syntax/nc.vim`
    to the _config file_ `~/.config/nvim/init.vim`
 3. Enable the highlighting with the VIM command `:set syntax=nc`

