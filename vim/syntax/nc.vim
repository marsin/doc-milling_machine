" Vim syntax file
"
"  Language: NC
"  Editor: Martin Singer <martin.singer@web.de>
"  Latest Change: 2019-07-02
"
"
" ## References
"
"  Syntax:
"
"  - http://wiki.linuxcnc.org/uploads/ngc.vim
"  - https://www.vim.org/scripts/script.php?script_id=4910
"
"
"  Vim regex:
"
"  - http://vimregex.com/
"
"
"  G-Code:
"
"  - http://linuxcnc.org/docs/html/gcode.html


" Quit when a (custom syntax file was already loaded
if exists("b:current_syntax")
    finish
endif


syntax case ignore


syntax match ncComment  /(.*)/ contains=ncTodo
syntax match ncComment  ";.*"

syntax keyword ncRapid  G0 G00
syntax keyword ncGCodes G01 G02 G03 G04 G1 G2 G3 G4 G10 G17 G18 G19 G20 G21 G28 G30 G33 G33.1 G38.2 G38.x G40 G41 G41.1 G42 G42.1 G43 G43.1 G49 G53 G54 G55 G56 G57 G58 G59 G59.1 G59.2 G59.3 G61 G61.1 G64 G76 G80 G81 G82 G83 G84 G85 G86 G87 G88 G89 G90 G91 G92 G92.1 G92.2 G92.3 G93 G94 G95 G96 G97 G98 G99
syntax keyword ncMCodes M00 M01 M02 M03 M04 M05 M06 M07 M08 M09 M0 M1 M2 M3 M4 M5 M6 M7 M8 M9 M30 M48 M49 M50 M51 M52 M53 M60 M62 M63 M64 M65 M66

syntax match ncKeyword "^%[a-z0-9_]*"
syntax keyword ncKeyword sub endsub while if repeat call

syntax match ncRoutines    "o[0-9]\{3}"
syntax match ncVariable    "#<[a-z][a-z0-9_]*>"
syntax match ncGVariable   "#<_[a-z][a-z0-9_]*>"
syntax match ncParameter   "#[0-9][0-9]*"
syntax match ncIndirection "[\[\]]"
syntax match ncNumber      "[+-]\=[0-9]\.\=[0-9]*"

syntax match ncXAxis       "[XY]"
syntax match ncZAxis       "Z"
syntax match ncAAxis       "[ABC]"
syntax match ncUAxis       "[UVW]"
syntax match ncIAxis       "[IJKRL]"

syntax match ncTool        "T"
syntax match ncFeed        "F"
syntax match ncSpindle     "S"


hi def link ncComment Comment

hi def link ncRapid  Special
hi def link ncGCodes Type
hi def link ncMCodes Constant

hi def link ncTool    Constant
hi def link ncFeed    Constant
hi def link ncSpindle Constant

hi def link ncXAxis Keyword
hi def link ncYAxis Keyword
hi def link ncZAxis Keyword

hi def link ncAAxis Identifier
hi def link ncIAxis Identifier
hi def link ncUAxis Identifier

hi def link ncRoutines Function
hi def link ncKeyword  Identifier

hi def link ncVariable  PreProc
hi def link ncGVariable PreProc

hi def link ncParameter PreProc
hi def link ncNumber    Ignonore

hi def link ncIndirection Constant

hi def link ncSpecials Special

let b:current_syntax = "nc"

