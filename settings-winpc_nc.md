# Basic Settings - WinPC-NC

 _Latest Update: 2019-07-04_


## X-Axis

### Machine area

 - __Axes resolution__:           400     _Steps/Rev._
 - __Distance per revolution__:     3.000 _mm/U_
 - __Maximum speed__:              50.000 _mm/s_
 - __Maximum start/stop speed__:    5.00  _mm/s_
 - __Rapid speed__:                30.00  _mm/s_
 - __Shortest ramp__:             300     _ms_
 - __Invert movement direction__: Yes
 - __Backlash__:                    0     _Steps_


## Y-Axis

### Machine area

 - __Axes resolution__:           400     _Steps/Rev._
 - __Distance per revolution__:     3.000 _mm/U_
 - __Maximum speed__:              50.000 _mm/s_
 - __Maximum start/stop speed__:    5.00  _mm/s_
 - __Rapid speed__:                30.00  _mm/s_
 - __Shortest ramp__:             300     _ms_
 - __Invert movement direction__: Yes
 - __Backlash__:                    0     _Steps_


## Z-Axis

### Machine area

 - __Axes resolution__:           400     _Steps/Rev._
 - __Distance per revolution__:     3.000 _mm/U_
 - __Maximum speed__:              50.000 _mm/s_
 - __Maximum start/stop speed__:    5.00  _mm/s_
 - __Rapid speed__:                30.00  _mm/s_
 - __Shortest ramp__:             300     _ms_
 - __Invert movement direction__:  No
 - __Backlash__:                    0     _Steps_


## Dimensions

### Machine area

 | Machine size | X       | Y       | Z       | Unit |
 | -----------: | ------: | ------: | ------: | :--- |
 |         from |    0.00 |    0.00 |    0.00 | _mm_ |
 |           to | +418.00 | +598.00 | +138.00 | _mm_ |


## Homing

 |                               | X     | Y       | Z     |        |
 | ----------------------------: | ----: | ------: | ----: | :----- |
 | __Homing switch at ... end__  | neg.  | pos.    | neg.  | End    |
 | __Homing point is ...__       | +0.00 | +598.00 | +0.00 | _mm_   |
 | __Homing offset__             | +0.00 |   +0.00 | +0.00 | _mm_   |
 | __Homing speed, search__      |  5.00 |   10.00 |  5.00 | _mm/s_ |
 | __Homing speed, moving free__ |  1.00 |    1.00 |  1.00 | _mm/s_ |


 - __Homing sequence__: z-x-y

 - [x] __Check home switches prior to machine initialization__


## Jog

 |                     | X     | Y     | Z     |        |
 | ------------------: | ----: | ----: | ----: | :----- |
 | __Jog speed, fast__ | 25.00 | 25.00 | 15.00 | _mm/s_ |
 | __Jog speed, slow__ |  5.00 |  5.00 |  2.50 | _mm/s_ |

 - [ ] __Jogging, reset to continuous__
 - __Changeover time__: 300 _ms_


## Signal Wizard

 | Inputs                      | Pinning    |
 | :-------------------------- | :--------- |
 | __I255__ Start              | _n/a_      |
 | __I254__ Stop               | _n/a_      |
 | __I247__ Not Ready          | LPT1 Pin11 |
 | __I235__ Homing switch X    | LPT1 Pin12 |
 | __I236__ Homing switch Y    | LPT1 Pin12 |
 | __I237__ Homing switch Z    | LPT1 Pin12 |
 | __I238__ Homing switch 4    | _n/a_      |
 | __I239__ Limit switch X-    | _n/a_      |
 | __I240__ Limit switch Y-    | _n/a_      |
 | __I241__ Limit switch Z-    | _n/a_      |
 | __I242__ Limit switch 4+    | _n/a_      |
 | __I243__ Limit switch X+    | _n/a_      |
 | __I244__ Limit switch Y+    | _n/a_      |
 | __I245__ Limit switch Z+    | _n/a_      |
 | __I246__ Limit switch 4+    | _n/a_      |
 | __I234__ ZinPos upper       | _n/a_      |
 | __I233__ ZinPos lower       | _n/a_      |
 | __I229__ Limit both X+-     | _n/a_      |
 | __I230__ Limit both Y+-     | _n/a_      |
 | __I231__ Limit both Z+-     | _n/a_      |
 | __I232__ Limit both 4+-     | _n/a_      |
 | __I228__ Spindle spd ok     | _n/a_      |
 | __I222__ Housing            | _n/a_      |
 | __I221__ Surface sensor     | LPT1 Pin10 |
 | __I220__ Homing switch Xb   | _n/a_      |
 | __I180__ JobSingle          | _n/a_      |
 | __I179__ JobStart           | _n/a_      |
 | __I178__ JobStop            | _n/a_      |
 | __I177__ JobResume          | _n/a_      |
 | __I176__ THC Move Z down    | _n/a_      |
 | __I175__ THC Move Z up      | _n/a_      |
 | __I199__ THC Fehler         | _n/a_      |
 | __I200__ THC Arc good       | _n/a_      |
 | __I201__ ATC 1 present      | _n/a_      |
 | __I202__ ATC 2 present      | _n/a_      |
 | __I203__ ATC 3 present      | _n/a_      |
 | __I204__ ATC 4 present      | _n/a_      |
 | __I205__ ATC 5 present      | _n/a_      |
 | __I206__ ATC 6 present      | _n/a_      |
 | __I207__ ATC 7 present      | _n/a_      |
 | __I208__ ATC 8 present      | _n/a_      |
 | __I209__ ATC 9 present      | _n/a_      |
 | __I210__ ATC 10 present     | _n/a_      |
 | __I181__ Error signal 1     | _n/a_      |
 | __I182__ Error signal 2     | _n/a_      |
 | __I183__ Error signal 3     | _n/a_      |
 | __I184__ Error signal 4     | _n/a_      |
 | __I185__ Error signal 5     | _n/a_      |
 | __I186__ Error signal 6     | _n/a_      |
 | __I187__ Error signal 7     | _n/a_      |
 | __I188__ Error signal 8     | _n/a_      |
 | __I189__ Error signal 9     | _n/a_      |
 | __I190__ Error signal 10    | _n/a_      |
 | __I160__ _free_             | _n/a_      |
 | __I161__ _free_             | _n/a_      |
 | __I162__ _free_             | _n/a_      |
 | __I163__ _free_             | _n/a_      |
 | __I164__ _free_             | _n/a_      |
 | __I165__ _free_             | _n/a_      |
 | __I166__ _free_             | _n/a_      |
 | __I167__ _free_             | _n/a_      |
 | __I168__ _free_             | _n/a_      |
 | __I169__ _free_             | _n/a_      |
 | __I100__ _free_             | _n/a_      |
 | __I101__ _free_             | _n/a_      |
 | __I102__ _free_             | _n/a_      |
 | __I103__ _free_             | _n/a_      |
 | __I104__ _free_             | _n/a_      |
 | __I105__ _free_             | _n/a_      |
 | __I106__ _free_             | _n/a_      |
 | __I107__ _free_             | _n/a_      |
 | __I108__ _free_             | _n/a_      |
 | __I109__ _free_             | _n/a_      |


 | Outputs                     | Pinning    |
 | :-------------------------- | :--------- |
 | __Q255__ Ready              | _n/a_      |
 | __Q251__ Axes moving        | _n/a_      |
 | __Q250__ Boost              | _n/a_      |
 | __Q242__ Spindle on/off     | LPT1 Pin1  |
 | __Q243__ Cooling on/off     | LPT1 Pin14 |
 | __Q244__ Dispensing/Laser   | _n/a_      |
 | __Q245__ Cleaning           | _n/a_      |
 | __Q246__ Job active         | _n/a_      |
 | __Q247__ Job end            | _n/a_      |
 | __Q248__ Molette            | _n/a_      |
 | __Q249__ Spindle left/right | _n/a_      |
 | __Q100__ Output M70         | _n/a_      |
 | __Q101__ Output M71         | _n/a_      |
 | __Q102__ Output M72         | _n/a_      |
 | __Q103__ Output M73         | _n/a_      |
 | __Q104__ Output M74         | _n/a_      |
 | __Q105__ Output M75         | _n/a_      |
 | __Q106__ Output M76         | _n/a_      |
 | __Q107__ Output M77         | _n/a_      |
 | __Q108__ Output M80         | _n/a_      |
 | __Q109__ Output M81         | _n/a_      |
 | __Q110__ Output M82         | _n/a_      |
 | __Q111__ Output M83         | _n/a_      |
 | __Q112__ Output M84         | _n/a_      |
 | __Q113__ Output M85         | _n/a_      |
 | __Q114__ Output M86         | _n/a_      |
 | __Q115__ Output M87         | _n/a_      |
 | __Q219__ Chargepump         | _n/a_      |
 | __Q218__ Sp.speed PWM       | LPT1Pin17  |
 | __Q217__ Profi-const spd    | _n/a_      |
 | __Q216__ Profi-Chargepump2  | _n/a_      |
 | __Q215__ Profi-PWM2         | _n/a_      |
 | __Q216__ THC ignition       | _n/a_      |
 | __Q213__ Pilotlaser/Camera  | _n/a_      |
 | __Q221__ _free_             | _n/a_      |
 | __Q222__ _free_             | _n/a_      |
 | __Q223__ _free_             | _n/a_      |
 | __Q224__ _free_             | _n/a_      |
 | __Q225__ _free_             | _n/a_      |
 | __Q226__ _free_             | _n/a_      |
 | __Q227__ _free_             | _n/a_      |
 | __Q228__ _free_             | _n/a_      |
 | __Q229__ _free_             | _n/a_      |
 | __Q230__ _free_             | _n/a_      |


 - _( )   USB_
 -  (x) __USB ST__
 - _( )   USB nc100_


## Spindle


 - Q242 Spindle on/off = LPT Pin1
 - Q218 Sp. speed PWM = LPT1 Pin17
 - I228 Spindle spd ok = n/a

 - __Maximum spindle speed__:    20000 _rpm_
 - __Spindle speed, default__:   10000 _rpm_
 - __Dwell time at spindle on__:  2000 _ms_


## Speed control

 - __Dynamic speed control__:  1 _(0..30)_
 - __Contour smooting__:      50 _(0..2000)_

 - [ ] __Set global brake angle__

