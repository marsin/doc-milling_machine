; # Test 04 - Drilling Cycle
;
;  - Latest edit: _2019-07-12_
;  - Latest test: _2019-07-11_
;
;
; ## About
;
;  This program tests the drilling cycle
;
;
; ### Tested Commands
;
;  - `G81` is a drilling cycle where the pullback speed is the same as the infeed speed
;  - `G82` is a drilling cycle where the pullback speed is in rapid speed
;
;
; ### Annotations
;
;  - The drilling cycle `G81` seems to behave like the `G82` drilling cycle
;    - The pullback speed of `G81` is much faster than the infeed speed
;    - There is no recognizable difference between the pullback speed of `G81` and `G82`
;
;
; ## Environment
;
;  - Control Software: WinPC-NC USB 3.0
;  - Machine:          Stepcraft 600
;    - __ATTENTION__: The Z axis is switched on this control software!
;                     Positive Z values feeds the Z axis downwards.
;
;  - Material:         Soft wood
;  - Tool:             Drill, 2 mm diameter
;
%test04

; Preamble
M05   ; set spindle off
T1 M6 ; select Tool1, switch tool
G71   ; set units to mm
G90   ; set absolute coordinates

; Goto start/end point (10 mm above zero point)
G00 X  0.000 Y0.000
G00 Z-10.000
M03 S5000           ; set spindle ON, 5000 rpm (25%), clockwise

; Work cycle
G00 X0.000 Y0.000
G81 Z25.000 R2.500 P1.000 ; drilling cycle, depth: 5 mm depth, pullback height: 2.5 mm, dwell: 1 sec
G00 X+50.000 Y0.000
G82 Z25.000 R2.500 P1.000 ; drilling cycle with pullback in rapid speed
G00 X+50.000 Y+50.000
G81 Z25.000 R2.500 P1.000
G00 X0.000 Y50.000
G82 Z25.000 R2.500 P1.000

; Goto start/end point
G90          ; set absolute coordinates
G00 Z-10.000

; Epilogue
M05 ; set spindle OFF
M30 ; program end

