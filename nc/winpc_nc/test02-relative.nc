; # Test 02 - Relative coordinates
;
;  Latest edit: _2019-07-10_
;
;
; ## About
;
;  This program test movement with relative coordinates
;
;
; ## Environment
;
;  - Control Software: WinPC-NC USB 3.0
;  - Machine:          Stepcraft 600
;    - __ATTENTION__: The Z axis is switched on this control software!
;                     Positive Z values feeds the Z axis downwards.
;
;  - Material:         Soft wood
;  - Tool:             End mill, 2 mm diameter
;
%test02

; Preamble
M05   ; set spindle off
T1 M6 ; select Tool1, switch tool
G71   ; set units to mm
G90   ; set absolute coordinates

; Goto start/end point (10 mm above zero point)
G00 X  0.000 Y0.000
G00 Z-10.000

; Goto zero point
M03 S5000       ; set spindle ON, 5000 rpm (25%), clockwise
G01 F2.5 Z0.000 ; feed to zero point

; Work cycle
G91               ; set relative coordinates
G01 F2.5 Z+ 5.000 ; dive in
G01 F5.0 X+25.000
G01 F5.0 Y+50.000
G01 F5.0 X-25.000
G01 F5.0 Y-50.000
G01 F5.0 Z- 5.000 ; dive out

; Goto start/end point
G90               ; set absolute coordinates
G01 F5.0 Z-10.000

; Epilogue
M05 ; set spindle OFF
M30 ; program end

