; # Test 01 - Movement with Absolute Coordinates
;
;  - Latest edit: _2019-07-10_
;
;
; ## About
;
;  This program tests basic movement with absolute coordinates
;
;
; ## Test Environment
;
; ### System
;
;  - Controller Hardware: Intel Celeron 2.4 GHz CPU, 1 GiB RAM
;  - Controller OS:       Microsoft Windows XP PRO, SP3
;  - Controller Software: Lewetz WinPC-NC USB 3.40/26 _(scUSB 55780)_
;  - Machine:             STEPCRAFT-2/D.600 Construction Kit
;  - Spindle:             STEPCRAFT HF Spindle 500 W
;  - Tool Changer:        _none_
;  - Tool Length Sensor:  _none_
;
;
; #### Controller Settings
;
;  - Coordinates of the Z axis are switched (+ := down; - := up)
;  - Feed rates in _mm/s_ or _inch/min_
;
;
; ### Tools & Material
;
;  - Tool:     _none_
;  - Material: _none_
;
;
; ## References
;
;  - https://www.lewetz.de/de/service/downloads
;

%test01

; Preamble
M05   ; set spindle off
T1 M6 ; select Tool 1, switch tool
G71   ; set units to mm
G90   ; set absolute coordinates

; Goto start/end point (10 mm above zero point)
G00 X  0.000 Y0.000
G00 Z-10.000

; Work cycle
M03 S5000         ; set spindle ON, 5000 rpm (25%), clockwise
G01 F2.5 Z+ 5.000 ; dive in, feed rate: 2.5 mm/s
G01 F5.0 X+25.000
G01 F5.0 Y+50.000
G01 F5.0 X  0.000
G01 F5.0 Y  0.000
G01 F5.0 Z-10.000 ; dive out, goto start/end point

; Epilogue
M05 ; set spindle OFF
M30 ; program end

