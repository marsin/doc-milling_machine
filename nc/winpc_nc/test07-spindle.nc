; # Test 07 - Spindle
;
;  - Latest edit: _2019-07-15_
;
;
; ## About
;
;  This program tests the behavior of the spindle commands.
;  Clockwise and counterclockwise rotation.
;  Increasing rotation speed without 'turn ON' command.
;  Rotation reversal without stop.
;
;
; ### Tested Commands
;
;  - `M03`: turn spindle ON, clockwise
;  - `M04`: turn spindle ON, counterclockwise
;  - `M05`: turn spindle OFF
;
;
; ### Evaluation
;
;  - Spindle can not rotate counterclockwise
;    On `M04`, the spindle rotates clockwise.
;  - When turning spindle OFF with `M05`, the controler does __not__ wait
;    until the spindle stands still but moves on in the program.
;  - When increasing (or lowering) the rotation speed of the spindle in two steps,
;    the controller waits for the first speed,
;    sets then the second speed and waits for this speed again.
;  - When the spindle speed is lower than then the minimum speed,
;    the spindle stands still and the controller moves on.
;  - A direct speed reverse (from forward directly to backward, without stop)
;    can not be tested, because this spindle has no counterclockwise rotation.
;
;
; ## Test Environment
;
; ### System
;
;  - Controller Hardware: Intel Celeron 2.4 GHz CPU, 1 GiB RAM
;  - Controller OS:       Microsoft Windows XP PRO, SP3
;  - Controller Software: Lewetz WinPC-NC USB 3.40/26 _(scUSB 55780)_
;  - Machine:             STEPCRAFT-2/D.600 Construction Kit
;  - Spindle:             STEPCRAFT HF Spindle 500 W
;  - Tool Changer:        _none_
;  - Tool Length Sensor:  _none_
;
;
; ### Tools & Material
;
;  - Tool:     _none_
;  - Material: _none_
;
;
; ## References
;
;  - https://www.lewetz.de/de/service/downloads
;

%test07     ; start program

G71         ; set units to mm
G90         ; set absolute coordinates

M05         ; turn spindle OFF
T01         ; select tool 1
M06         ; change tool

G17         ; select plane XY
F5          ; set feed rate to 5 mm/s (WinPC-NC)

G00 X0 Y0   ; goto zero point with X and Y axis
G00 Z0      ; goto zero point with Z axis
L1          ; call subroutine
G00 X0 Y0
G02 X0 Y0 I2.5 ; move arc, (indicates the start of the second routine)
L2
G00 X0 Y0

M05         ; turn spindle OFF
M30         ; end program


G98 L1
S2000       ; set spindle speed to 2000 rpm (10%), but no spindle turn ON command
G01 X10 Y0
M03         ; turn spindle ON, clockwise
G01 X10 Y10
M05         ; turn spindle OFF
G01 X0  Y10
M04         ; turn spindle ON, counterclockwise
G01 X0  Y0
M05         ; turn spindle OFF
G98 L0


G98 L2
S2000       ; set spindle speed to 2000 rpm (minimum, 10%)
G01 X10 Y0
M03         ; turn spindle ON, clockwise
G01 X10 Y10
S5000       ; increase spindle speed (5000 rpm, 25%)
G01 X0  Y10
S10000      ; increase spindle speed (10000 rpm, 50%) ...
S2500       ;  ... and lower again directly after increasing (2500 rpm, 12.5%)
G01 X0  Y0
M04         ; turn spindle ON, counterclockwise (reverse without stop)
G01 X10 Y0
S20000      ; set maximum spindle speed (20000 rpm, 100%)
G01 X10 Y10
S2000       ; set minimum spindle speed (2000 rpm, 10%)
G01 X0 Y10
S1000       ; set spindle speed to lower than the minimum (1000 rpm, 5%)
G01 X0 Y0
M05         ; turn spindle OFF
G98 L0

