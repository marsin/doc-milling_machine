; # Test 08 - Wait for Input
;
;  - Latest edit: _2019-07-16_
;
;
; ## About
;
;  This program tests the dwell, wait and halt commands.
;
;
; ### Tested Commands
;
;  - `G04`: dwell
;  - `M16`: wait for input signal
;  - `M00`: program halt
;
;
; #### Tested Command Parameters
;
;  - `G04`:
;    - `F`: delay time in milliseconds
;  - `M16`:
;    - `F`: input number
;
;
; ### Evaluation
;
;  - Command `G04` works, parameter `F` is in milliseconds
;  - Both `M16` commands are ignored,
;    probably because Input 1 and 2 are not configured in the controller
;  - Command `M00` works, a button "Continue" appears on the controller GUI
;
; ## Test Environment
;
; ### System
;
;  - Controller:         WinPC-NC USB 3.0
;  - Machine:            STEPCRAFT-2/D.600 Construction Kit
;  - Spindle:            STEPCRAFT HF Spindle 500 W
;  - Tool Changer:       _none_
;  - Tool Length Sensor: _none_
;
;
; ### Tools & Material
;
;  - Tool:     _none_
;  - Material: _none_
;
;
; ## References
;
;  - https://www.lewetz.de/de/service/downloads
;  - http://linuxcnc.org/docs/html/gcode/g-code.html#gcode:g4
;  - http://linuxcnc.org/docs/html/gcode/m-code.html#mcode:m0-m1
;

%test08

M05 ; turn spindle OFF
T01 ; select Tool 1
M06 ; switch tool

G71 ; set units to mm
G90 ; set absolute coordinates

G17 ; select plane XY
F5  ; set feed rate to 5 mm/s (WinPC-NC)

G00 X0   Y0 ; goto zero point with X and Y axis
G00 Z-10    ; goto start/end point with Z axis

S5000 ; set spindle speed to 5000 rpm (25%)
M03   ; turn spindle ON, clockwise


G01 Z5
G04 F5000 ; dwell for 5 seconds
G01 Z-10
M05
M16 F1 ; wait for input number 1
M03
M16 F2 ; wait for input number 2
M05
G01 X10
M00    ; halt program
M03
G01 X0


M05 ; turn spindle OFF
M30 ; end program

