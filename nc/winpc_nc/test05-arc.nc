; # Test 05 - Arc move
;
;  - Latest edit: _2019-07-15_
;
;
; ## About
;
;  This program tests the arc moves.
;
;
; ### Tested Commands
;
;  - `G17`: select plane XY, rotation axis Z
;  - `G18`: select plane XZ, rotation axis Y
;  - `G19`: select plane YZ, rotation axis X
;  - `G02`: move arc, clockwise
;  - `G03`: move arc, counterclockwise
;
;
; #### Tested Command Parameters
;
;  - for plane XY (`G17`):
;    - `I`: arc center point is on X axis, seen from the start point
;    - `J`: arc center point is on Y axis, seen from the start point
;
;  - for plane XZ (`G18`):
;    - `I`: arc center point is on X axis, seen from the start point
;    - `K`: arc center point is on Z axis, seen from the start point
;
;  - for plane YZ (`G19`):
;    - `J`: arc center point is on Y axis, seen from the start point
;    - `K`: arc center point is on Z axis, seen from the start point
;
;
; ## Test Environment
;
; ### System
;
;  - Controller Hardware: Intel Celeron 2.4 GHz CPU, 1 GiB RAM
;  - Controller OS:       Microsoft Windows XP PRO, SP3
;  - Controller Software: Lewetz WinPC-NC USB 3.40/26 _(scUSB 55780)_
;  - Machine:             STEPCRAFT-2/D.600 Construction Kit
;  - Spindle:             STEPCRAFT HF Spindle 500 W
;  - Tool Changer:        _none_
;  - Tool Length Sensor:  _none_
;
;
; ### Tools & Material
;
;  - Tool:     _none_
;  - Material: _none_
;
;
; ## References
;
;  - https://www.lewetz.de/de/service/downloads
;  - http://linuxcnc.org/docs/html/gcode/g-code.html#gcode:g2-g3
;

%test05            ; start program

G71                ; set units to mm (WinPC-NC)
G90                ; set absolute coordinates

T1                 ; select tool 1
M06                ; change tool

;G28                ; goto zero point
G00 X0   Y0        ; goto zero point with X and Y

S5000              ; set spindle speed to 5000 rpm (25%)
M03                ; set spindle ON, clockwise

G17                ; select plane XY
F5                 ; set feed rate to 5 mm/s (WinPC-NC)

L1                 ; call subroutine 1
L2

M05                ; set spindle OFF
M30                ; end program


; Subroutine 1: Shape with rounded rectangular corners
G98 L1
G00 X10  Y0   Z-5  ; goto start/end point
G01 Z5             ; infeed
G01 X90  Y0
G03 X100 Y10  J10  ; move arc, counterclockwise, from X90  Y0  to X100 Y10,  center point is X90  Y10 (J10  := center point is  10 [mm] in Y direction)
G01 X100 Y40
G02 X110 Y50  I10  ; move arc, clockwise,        from X100 Y40 to X110 Y50,  center point is X110 Y40 (I10  := center point is  10 [mm] in X direction)
G03 X120 Y60  J10  ; move arc, counterclockwise, from X110 Y50 to X120 Y60,  center point is X110 Y60
G01 X120 Y90
G03 X110 Y100 I-10 ; move arc, counterclockwise, from X120 Y90 to X110 Y100, center point is X110 Y90 (I-10 := center point is -10 [mm] in X direction)
G02 X100 Y110 J10
G01 X100 Y140
G03 X90  Y150 I-10
G01 X10  Y150
G03 X0   Y140 J-10
G01 X0   Y10
G03 X10  Y0   I10
G01 Z-5            ; feed out to start/end point
G98 L0


; Subroutine 2: Shape with not rectangular arcs
G98 L2
G00 X0   Y0   Z-5       ; goto start/end point
G01 Z5                  ; infeed
G02 X50  Y50  I75  J-25
G03 X100 Y100 I-25 J75
G02 X100 Y100 I5   J5
G03 X50  Y50  I25  J-75
G02 X0   Y0   I-75 J25
G01 Z-5                 ; feed out to start/end point
G98 L0

