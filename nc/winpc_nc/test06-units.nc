; # Test 06 - Units
;
;  - Latest edit: _2019-07-15_
;
;
; ## About
;
;  This test shows that the common commands `G20` and `G21`
;  for setup the units inch or mm, are __not__ working
;  as the controller specific commands `G70` and `G71`.
;
;
; ### Tested Commands
;
;  - `G70`: set unit _inch_ _(WinPC-NC)_
;  - `G71`: set unit _mm_   _(WinPC-NC)_
;
;  - `G20`: set unit _inch_ _(LinuxCNC, other)_
;  - `G21`: set unit _mm_   _(LinuxCNC, other)_
;
;
; ## Test Environment
;
; ### System
;
;  - Controller Hardware: Intel Celeron 2.4 GHz CPU, 1 GiB RAM
;  - Controller OS:       Microsoft Windows XP PRO, SP3
;  - Controller Software: Lewetz WinPC-NC USB 3.40/26 _(scUSB 55780)_
;  - Machine:             STEPCRAFT-2/D.600 Construction Kit
;  - Spindle:             STEPCRAFT HF Spindle 500 W
;  - Tool Changer:        _none_
;  - Tool Length Sensor:  _none_
;
;
; ### Tools & Material
;
;  - Tool:     _none_
;  - Material: _none_
;
;
; ## References
;
;  - https://www.lewetz.de/de/service/downloads
;  - http://linuxcnc.org/docs/html/gcode/g-code.html#gcode:g20-g21
;

%test05   ; start program

G71       ; set units to mm (WinPC-NC)
G90       ; set absolute coordinates

T1        ; select tool 1
M06       ; change tool

G00 X0 Y0 ; goto zero point with X and Y
G00 Z0    ; goto zero point with Z

L1        ; call subroutine
G71       ; set units to mm (WinPC-NC)
G00 Y10   ; goto 10 mm to the side
L2
G71
G00 Y20
L3
G71
G00 Y30
L4

M05       ; set spindle OFF
M30       ; end program


G98 L1     ; Subroutine 1
G71        ; set units to mm (WinPC-NC)
S5000      ; set spindle speed to 5000 rpm (25%)
M03        ; set spindle ON, clockwise
F25.4      ; set feed rate to 25.4 mm/s (WinPC-NC)
G01 X 25.4 ; feed 25.4 mm (1 inch)
G01 X 0    ; and back
G98 L0


G98 L2
G70        ; set units to inch (WinPC-NC)
S5000
M03
F1         ; set feed rate to 1 inch/s (WinPC-NC)
G01 X 1    ; feed 1 inch
G01 X 0
G98 L0


G98 L3
G70        ; set units to inch (WinPC-NC, contrary to the following command)
G21        ; set units to mm (LinuxCNC)
S5000
M03
F25.4      ; set feed rate to 25.4 mm/s (WinPC-NC)
G01 X 25.4 ; feed 25.4 mm (1 inch)
G01 X 0
G98 L0


G98 L4
G71        ; set units to mm (WinPC-NC, contrary to the following command)
G20        ; set units to inch (LinuxCNC)
S5000
M03
F1         ; set feed rate to 1 inch/s (WinPC-NC)
G01 X 1    ; feed 1 inch
G01 X 0
G98 L0

