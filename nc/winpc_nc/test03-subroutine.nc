; # Test 03 - Subroutines
;
;  Latest edit: _2019-07-10_
;
;
; ## About
;
;  This program is a first subroutine test.
;
;
; ### Annotations
;
;  - It is important to select a tool and switch the tool _(`T1 M6`)_,
;    because if this is not done, the controller software
;    does no rapid moves with the rotating spindle.
;    - It passes the subroutines.
;    - Had some strange behavior with relative coordinates
;      in an other program - maybe because of lacking the tool selection.
;
;
; ## Environment
;
;  - Control Software: WinPC-NC USB 3.0
;  - Machine:          Stepcraft 600
;    - __ATTENTION__: The Z axis is switched on this control software!
;                     Positive Z values feeds the Z axis downwards.
;
;  - Material:         Soft wood
;  - Tool:             End mill, 2 mm diameter
;
%test03

M05 ; set spindle off
T1 M6 ; select Tool1, switch tool
G71 ; set units to mm
G90 ; set absolute coordinates

; Goto start/end point (10 mm above zero point)
G00 X  0.000 Y0.000
G00 Z-10.000
M03 S5000           ; set spindle ON, 5000 rpm (25%), clockwise

; Work cycle
G00 X   0.000 Y+10.000 ; goto position X, Y
L1                     ; call Subroutine1
G00 X +50.000 Y+10.000
L1
G00 X+100.000 Y+10.000
L1

; Goto start/end point
G90               ; set absolute coordinates
G01 F5.0 Z-10.000

; Epilogue
M05 ; set spindle OFF
M30 ; program end


; Subroutine1: Mill Rectangle
G98 L1            ; defining subroutine
G90               ; set absolute coordinates
G01 F2.5 Z  0.000 ; feed down to zero point
G91               ; set relative coordinates
G01 F2.5 Z +5.000 ; dive in
G01 F5.0 X+25.000
G01 F5.0 Y+50.000
G01 F5.0 X-25.000
G01 F5.0 Y-50.000
G01 F5.0 Z -5.000 ; dive out
G90               ; set absolute coordinates
G01 F5.0 Z -2.500 ; feed up to 2.5 mm above the workpiece
G98 L0            ; end subroutine

