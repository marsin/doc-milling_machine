# Tool Selection

 _(Source: Spindel HF350 / HF500, Bedien- / Sicherheitshinweise, Stand: 2017-08-22, Kapitel 4.6 - Tabelle, Seite 19)_


 | Material                         | Milling Cutter Type                  |
 | :------------------------------- | :----------------------------------- |
 | Softwood                         | Cutter with Spiral Teeth             |
 | Hardwood                         | Cutter with Spiral Teeth             |
 | Carbon fiber                     | Cutter with Diamond or Spiral Teeth  |
 | Glass fiber strengthened plastic | Cutter with Diamond or Spiral Teeth  |
 | Soft plastic                     | Cutter with Spiral Groove (1-Cutter) |
 | Hard plastic                     | Cutter with Spiral Groove (1-Cutter) |
 | Acrylic glass                    | Cutter with Fish Tail Cut (2-Cutter) |
 | Aluminium (lead alloys)          | Cutter with Fish Tail Cut (2-Cutter) |
 | Brass                            | Cutter with Fish Tail Cut (2-Cutter) |


## Rotation and Feeding Rates

 _(Source: Spindel HF350 / HF500, Bedien- / Sicherheitshinweise, Stand: 2017-08-22, Kapitel 4.6 - Tabelle, Seite 19)_


 - __MPD__:      Maximum Plunging Depth per Cycle _[mm]_
 - __RPM__:      Spindle Rotations per Minute     _[1000 x n/Min]_
 - __V horiz.__: Horizontal Feedrate (X / Y)      _[mm/s]_
 - __V vert.__:  Verical Feedrate    (Z)          _[mm/s]_


### Milling Cutter: Diameter 1 mm

 | Material                         | MPD | RPM | V vert. | V horiz. |
 | :------------------------------- | --: | --: | ------: | -------: |
 | Softwood                         | 5   |  20 |       4 |       12 |
 | Hardwood                         | 2   |  18 |       3 |        4 |
 | Carbon fiber                     | 3   |  20 |       3 |        4 |
 | Glass fiber strengthened plastic | 3   |  20 |       3 |        4 |
 | Soft plastic                     | 5   |  12 |       4 |        4 |
 | Hard plastic                     | 2   |  18 |       3 |        3 |
 | Acrylic glass                    | 2   |  15 |       1 |        1 |
 | Aluminium (lead alloys)          | 0.5 |  20 |       1 |        2 |
 | Brass                            | 0.5 |  20 |       1 |        2 |


### Milling Cutter: Diameter 2 mm

 | Material                         | MPD | RPM | V vert. | V horiz. |
 | :------------------------------- | --: | --: | ------: | -------: |
 | Softwood                         |   6 |  15 |       5 |       18 |
 | Hardwood                         |   3 |  12 |       4 |        6 |
 | Carbon fiber                     |   3 |  16 |       4 |        4 |
 | Glass fiber strengthened plastic |   3 |  16 |       4 |        4 |
 | Soft plastic                     |   6 |  12 |       5 |        6 |
 | Hard plastic                     |   3 |  14 |       4 |        5 |
 | Acrylic glass                    |   3 |  12 |       2 |        5 |
 | Aluminium (lead alloys)          |   1 |  17 |       1 |        2 |
 | Brass                            |   1 |  17 |       1 |        2 |



### Milling Cutter: Diameter 3 mm

 | Material                         | MPD | RPM | V vert. | V horiz. |
 | :------------------------------- | --: | --: | ------: | -------: |
 | Softwood                         |   8 |  10 |       5 |       14 |
 | Hardwood                         |   4 |   8 |       4 |        5 |
 | Carbon fiber                     |   2 |  12 |       4 |        3 |
 | Glass fiber strengthened plastic |   2 |  12 |       4 |        3 |
 | Soft plastic                     |   8 |   6 |       5 |        6 |
 | Hard plastic                     |   4 |  10 |       3 |        4 |
 | Acrylic glass                    |   3 |   8 |       2 |        4 |
 | Aluminium (lead alloys)          |   1 |  14 |       1 |        2 |
 | Brass                            |   1 |  14 |       1 |        2 |



