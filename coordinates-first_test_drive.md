### First Test Drive

 _Date: 2019-02-27_


 The Coordinates of my first test drive.
 Done with the 'WinPC-NC' software.

 - After reference drive:
   - Machine coordinates _[mm]_:
     - __X__:   0.000
     - __Y__: 597.997
     - __Z__:   0.000

   - Work piece coordinates _[mm]_:
     - __X__: -10.000
     - __Y__: 587.997
     - __Z__:  -1.000

 - After zero point drive:
   - Machine coordinates _[mm]_:
     - __X__:  10.005
     - __Y__:  10.005
     - __Z__:   0.997

   - Work piece coordinates _[mm]_:
     - __X__:   0.005
     - __Y__:   0.005
     - __Z__:   0.003

 - Max X test
   - Machine coordinates _[mm]_:
     - __X__: 418.005

   - Work piece coordinates _[mm]_:
     - __X__: 408.005

